from SparseTable import SparseTable

class FamilyTree:
	###
	# This is the main class for handling family trees.
	###
	def __init__(self, name, numVertices, persons, edges):
		###
		# Builds up a Family Tree.
		# Inputs:
		# 	numVertices - int number of persons in tree
		# 	persons - list of Nodes of data, 1st person is root
		# 	edges - list of relationships in form (a, b) representing a --> b
		# Assembly:
		# 	Check 1 - declared count = actual count
		# 	Check 2 - E=V-1
		# 	Form the personMap - names --> Nodes
		# 		Check 3 - Node info is complete
		# 		Check 4 - name is not duplicate
		# 	Set root
		# 	Check 5/6 - degree check: outdegrees <= 2, indegrees = 1 if not root, 0 if root
		# 				endpoint check: ends of edges must be to existent nodes
		# 	Link all relationships
		# 		also serves as 2nd pass for outdegree checks
		# 	Check 7 - traversal check: perform inorder traversal, must reach everything
		# 		traversal check will also be used later to preprocess LCA
		# 	Set generation numbers beginning from root
		# 	Set up LCA arrays
		# 		traversal: indexNum --> name
		# 		revTraversal: name --> indexNum
		# 		depth: indexNum --> generations
		# 			LCA will run Sparse Table from this array
		# 		initialize Sparse Table from depth array
		###
		
		#print(name, numVertices, persons, edges)

		self.name = name
		self.size = numVertices
		self.numLinks = len(edges)
		self.isValid = True

		if self.size != len(persons):
			self.isValid = False
			print("Error: declared count does not match number of person entries")
			return

		if self.size != self.numLinks + 1: #either not full tree, or cycle exists
			self.isValid = False
			print("Error: family tree is ill-formed - family is not a tree")
			return

		self.personMap = dict()
		for person in persons: #attempt to form personmap
			if not person.isValid(): #incomplete info error
				self.isValid = False
				print("Error: person details are insufficient")
				return

			name = person.getName()
			if name in self.personMap: #duplicate name error
				self.isValid = False
				print("Error: person " + name + " already exists")
				return
			self.personMap[name] = person

		self.root = persons[0]

		indegree = {i : 0 for i in self.personMap.keys()}
		outdegree = {i : 0 for i in self.personMap.keys()}
		for edge in edges:
			a = edge[0]
			b = edge[1]
			if a not in self.personMap or b not in self.personMap:
				self.isValid = False
				print("Error: attempted to link nonexistent person(s)")
				return
			indegree[b] += 1
			outdegree[a] += 1
		# print(indegree)
		# print(outdegree)
		for k in outdegree.keys():
			if outdegree[k] > 2: #violate binary tree
				self.isValid = False
				print("Error: family tree is ill-formed - person " + k + " has too many children")
				return
		for k in indegree.keys():
			if k == self.root.getName():
				if indegree[k] != 0:
					self.isValid = False
					print("Error: family tree is ill-formed - root node must have no parents")
					return
			else:
				if indegree[k] != 1:
					self.isValid = False
					print("Error: family tree is ill-formed - non root nodes must have exactly 1 parent")
					return

		for edge in edges:
			a = edge[0]
			b = edge[1]
			if not self.personMap[a].addChild(self.personMap[b]):
				self.isValid = False
				print("Error: family tree is ill-formed - person " + a + " has too many children")
				return
		
		self.traversal = self.root.inorderTraverse()
		if len(self.traversal) != self.size:
			self.isValid = False
			print("Error: family tree is ill-formed - nodes unreachable from root")
			return

		self.root.setGeneration(0)
		self.revTraversal = {self.traversal[i] : i for i in range(len(self.traversal))}
		self.depth = [self.personMap[i].getGeneration() for i in self.traversal]
		self.sparse = SparseTable(self.depth)
	
	def valid(self):
		return self.isValid

	def getSize(self):
		#Implements 'count' command
		return self.size

	def getAge(self, name):
		#Implements 'age [name]' command
		if name in self.personMap:
			return self.personMap[name].getAge()
		else:
			return "Error: no such person " + name

	def getSex(self, name):
		#Implements 'sex [name]' command
		if name in self.personMap:
			s = self.personMap[name].getSex()
			return 'male' if s else 'female'
		else:
			return "Error: no such person " + name

	def getAncestors(self, name):
		#Implements 'ancestors [name]' command
		if name in self.personMap:
			l = sorted(self.personMap[name].getAncestors())
			if not l:
				return 'no ancestors'
			else:
				return '\n'.join(l)
		else:
			return "Error: no such person " + name

	def getDescendants(self, name):
		#Implements 'descendants [name]' command
		if name in self.personMap:
			l = sorted(self.personMap[name].getDescendants())
			if not l:
				return 'no descendants'
			else:
				return '\n'.join(l)
		else:
			return "Error: no such person " + name

	def setName(self, target, newName):
		#Implements 'edit-name [target] [newName]' command
		if target in self.personMap and newName not in self.personMap:
			targetNode = self.personMap[target]
			idx = self.revTraversal[target]
			targetNode.setName(newName)
			self.personMap.pop(target)
			self.personMap[newName] = targetNode
			self.traversal[idx] = newName
			self.revTraversal.pop(target)
			self.revTraversal[newName] = idx
		elif target not in self.personMap:
			print("Error: no such person " + target)
		elif newName in self.personMap:
			print("Error: name " + newName + " already in use")

	def setAge(self, target, newAge):
		#Implements 'edit-age [target] [newAge]' command
		if target in self.personMap:
			self.personMap[target].setAge(newAge)
		else:
			print("Error: no such person " + target)

	def setSex(self, target, newSex):
		#Implements 'edit-sex [target] [newSex]' command
		#Accepts: newSex - 'male' or 'female' only, not case-sensitive
		newSex = newSex.lower()
		if newSex == 'male':
			newSex = True
		elif newSex == 'female':
			newSex = False
		else:
			print("Error: unrecognized keyword " + newSex)
		if target in self.personMap:
			self.personMap[target].setSex(newSex)
		else:
			print("Error: no such person " + target)

	def getRelationship(self, a, b):
		#Implements 'relationship [a] [b]' command
		if a == b:
			return "Error: inputs must be distinct"
		if a not in self.personMap:
			return "Error: no such person " + name
		if b not in self.personMap:
			return "Error: no such person " + name
		def term(kind, d1, d2, s):
			if kind == 'child':
				if d1 < d2:
					if s:
						return 'son'
					else:
						return 'daughter'
				else:
					if s:
						return 'father'
					else:
						return 'mother'
			elif kind == 'nibling':
				if d1 < d2:
					if s:
						return 'nephew'
					else:
						return niece
				else:
					if s:
						return 'uncle'
					else:
						return 'aunt'
			return ''
		def ordinal(n):
			x = ['', 'first', 'second', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth']
			return x[n]
		def times(n):
			x = ['', 'once', 'twice', 'thrice', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten']
			if n >= 4:
				return x[n] + ' times'
			else:
				return x[n]
		def great(n):
			if n == 1:
				return ''
			return '-'.join(['great' for i in range(n-1)]) + '-'

		l = self.lowestCommonAncestor(a, b)
		d1 = self.personMap[a].getGeneration() - self.personMap[l].getGeneration()
		d2 = self.personMap[b].getGeneration() - self.personMap[l].getGeneration()
		near = min(d1, d2)
		far = max(d1, d2)
		if near >= 2: #cousins
			deg = near - 1
			rem = abs(d1-d2)
			if rem == 0:
				return ordinal(deg) + ' cousin'
			else:
				return ordinal(deg) + ' cousin ' + times(deg) + ' removed'
		elif near == 1:
			if far == 1:
				if self.personMap[b].getSex():
					return 'brother'
				else:
					return 'sister'
			elif far == 2: #aunt/uncle/niece/nephew
				return term('nibling', d1, d2, self.personMap[b].getSex())
			else: #grand+++ uncle/aunt/niece/nephew
				return great(far-1) + 'grand' + term('nibling', d1, d2, self.personMap[b].getSex())
		elif near == 0:
			if far == 1: #father/mother/son/daughter
				return term('child', d1, d2, self.personMap[b].getSex())
			else:
				return great(far-1) + 'grand' + term('child', d1, d2, self.personMap[b].getSex())

	def lowestCommonAncestor(self, a, b):
		#Helper method to obtain LCA of persons a and b
		a = self.revTraversal[a]
		b = self.revTraversal[b]
		if a > b:
			a, b = b, a
		lca = self.sparse.rmqLocation(a, b)
		return self.traversal[lca]

	def generateSaveFile(self):
		#Creates the text for this tree's savefile
		out = []
		out.append(['family', self.name])
		out.append(['count', str(self.size)])
		out.append(['name', self.root.getName()])
		out.append(['age', str(self.root.getAge())])
		out.append(['sex', str(self.root.getSex()).lower()])
		for node in self.personMap.values():
			if node.getName() != self.root.getName():
				out.append(['name', node.getName()])
				out.append(['age', str(node.getAge())])
				out.append(['sex', str(node.getSex()).lower()])
		out.append(['links', str(self.numLinks)])
		for node in self.personMap.values():
			if node.getLeft():
				out.append(['child', node.getName(), node.getLeft().getName()])
			if node.getRight():
				out.append(['child', node.getName(), node.getRight().getName()])
		# print(out)
		out = [' '.join(s) for s in out]
		return '\n'.join(out)




class Node:
	###
	# This class carries all the code for a Node in a FamilyTree.
	###
	def __init__(self, name=None, age=None, sex=None):
		self.name = name
		self.age = age
		self.sex = sex
		self.generation = -1
		self.l = None
		self.r = None
		self.p = None

	def __repr__(self):
		return str((self.name, (self.age), self.sex))

	def isValid(self):
		###
		# Checks validity for FamilyTree validation
		###
		a = self.name is not None
		b = self.age is not None
		c = self.sex is not None
		return a and b and c

	def addChild(self, child):
		###
		# Attempts to add child as a child and returns True.
		# If not possible, return False to proc error message
		###
		#print('add child to', self.name)
		if self.l is None:
			self.l = child
			self.l.p = self
			return True
		elif self.r is None:
			self.r = child
			self.r.p = self
			return True
		else:
			return False

	def inorderTraverse(self):
		###
		# Returns a list containing the inorder traversal from this node.
		###
		left = []
		center = [self.name]
		right = []
		if self.l is not None:
			left = self.l.inorderTraverse()
		if self.r is not None:
			right = self.r.inorderTraverse()
		return left + center + right

	def getDescendants(self):
		###
		# Returns a list containing all of this Node's descendants.
		###
		left = []
		right = []
		if self.l is not None:
			left = self.l.inorderTraverse()
		if self.r is not None:
			right = self.r.inorderTraverse()
		return left + right

	def getAncestors(self):
		###
		# Returns a list containing all of this Node's ancestors.
		###
		if self.p is not None:
			return [self.p.getName()] + self.p.getAncestors()
		else:
			return []

	def getName(self):
		return self.name

	def getAge(self):
		return self.age

	def getSex(self):
		return self.sex

	def getParent(self):
		return self.p

	def getLeft(self):
		return self.l

	def getRight(self):
		return self.r

	def getGeneration(self):
		return self.generation

	def setName(self, name):
		self.name = name

	def setAge(self, age):
		self.age = age

	def setSex(self, sex):
		self.sex = sex

	def setGeneration(self, gen):
		self.generation = gen
		if self.l is not None:
			self.l.setGeneration(gen+1)
		if self.r is not None:
			self.r.setGeneration(gen+1)
