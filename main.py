from FamilyTree import *

currentFile = None
family = None

def load(file):
	###
	# Reads the contents of the provided ft file and creates a FamilyTree.
	###
	familyName = ''
	num = 0
	numEdges = 0
	persons = []
	links = []

	line = file.readline()
	line = line.split()
	if line[0] != 'family':
		print('Error: expected keyword family instead of', line[0])
		return
	familyName = line[1]

	line = file.readline()
	line = line.split()
	if line[0] != 'count':
		print('Error: expected keyword count instead of', line[0])
		return
	num = int(line[1])

	for __ in range(num):
		line = file.readline()
		line = line.split()
		if line[0] != 'name':
			print('Error: expected keyword name instead of', line[0])
			return
		name = line[1]

		line = file.readline()
		line = line.split()
		if line[0] != 'age':
			print('Error: expected keyword age instead of', line[0])
			return
		age = int(line[1])

		line = file.readline()
		line = line.split()
		if line[0] != 'sex':
			print('Error: expected keyword sex instead of', line[0])
			return
		sex = line[1]
		if sex == 'true':
			sex = True
		elif sex == 'false':
			sex = False
		else:
			print('Error: unrecognized keyword', sex)
			return

		persons.append(Node(name, age, sex))

	line = file.readline()
	line = line.split()
	if line[0] != 'links':
		print('Error: expected keyword links instead of', line[0])
		return
	numEdges = int(line[1])

	for __ in range(numEdges):
		line = file.readline()
		line = line.split()
		if line[0] != 'child':
			print('Error: expected keyword child instead of', line[0])
			return
		if len(line) < 3:
			print('Error: 3 parameters required - check your FT file')
		links.append((line[1], line[2]))

	family = FamilyTree(familyName, num, persons, links)
	if not family.valid():
		family = None
		print('Load failed. Check your FT file for errors specified above.')
	else:
		print('Load successful.')
	return family



while True:
	###
	# Main loop to process commands.
	###
	inp = input('> ').split(None, 1)
	command = inp[0]
	params = None
	if len(inp) > 1:
		params = inp[1]

	proper = ['bye', 'load', 'save', 'count', 'age', 'sex', 'ancestors', 'descendants', 'edit-name', 'edit-age', 'edit-sex', 'relationship']
	
	if command not in proper:
		print('Error: unrecognized command', command)
	elif command == 'bye':
		print("Goodbye!")
		break
	elif command == 'load':
		try:
			currentFile = open(params + '.ft')
		except FileNotFoundError:
			print("Error: file does not exist")
		if currentFile:
			family = load(currentFile)
	elif currentFile is None:
		print("Error: a family tree must be loaded in the current session first")
	elif command == 'save':
		out = open(params + '.ft', mode='w')
		out.write(family.generateSaveFile())
		out.close()
	elif command == 'count':
		print(family.getSize())
	elif command == 'age':
		print(family.getAge(line[1]))
	elif command == 'sex':
		print(family.getSex(line[1]))
	elif command == 'ancestors':
		print(family.getAncestors(line[1]))
	elif command == 'descendants':
		print(family.getDescendants(line[1]))
	elif command == 'edit-name':
		params = params.split()
		if len(params) < 2:
			print('Error: 2 parameters required')
			continue
		family.setName(params[0], params[1])
	elif command == 'edit-age':
		params = params.split()
		if len(params) < 2:
			print('Error: 2 parameters required')
			continue
		family.setAge(params[0], params[1])
	elif command == 'edit-sex':
		params = params.split()
		if len(params) < 2:
			print('Error: 2 parameters required')
			continue
		family.setSex(params[0], params[1])
	elif command == 'relationship':
		params = params.split()
		if len(params) < 2:
			print('Error: 2 parameters required')
			continue
		print(family.getRelationship(params[0], params[1]))