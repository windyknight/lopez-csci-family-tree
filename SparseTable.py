from math import log2

class SparseTable:
	def __init__(self, ar):
		###
		# Assembles a Sparse Table to quickly compute RMQs.
		# Can also compute locations of RMQ values.
		# Complexity:
		# 	Setup: O(n log n)
		# 	Query: O(1)
		###
		self.ar = ar
		self.table = [[ar[i]] for i in range(len(ar))] #contains RMQs
		self.loc = [[i] for i in range(len(ar))] #contains RMQ locations
		idx = 1
		while (1<<idx) <= len(self.ar):
			for i in range(len(ar)): 
				#compute value at [i][idx]: rmq(i, i + (1<<idx) - 1)
				#compute as min(rmq(i, i+(1<<(idx-1))-1), rmq(i+(1<<(idx-1)), i+(1<<idx)-1))
				#in short, min(table[i][idx-1], table[i+(1<<(idx-1))][idx-1])
				#print('build:', str(i), str(idx))
				#print(str(i), str(idx-1))
				if idx-1 >= len(self.table[i]):
					continue
				a = self.table[i][idx-1]
				la = self.loc[i][idx-1]
				if i + (1 << (idx-1)) >= len(ar):
					continue
				if idx-1 >= len(self.table[i + (1 << (idx-1))]):
					continue
				#print(str(i + (1 << (idx-1))), str(idx-1))
				b = self.table[i + (1 << (idx-1))][idx-1]
				lb = self.loc[i + (1 << (idx-1))][idx-1]
				if a <= b:
					self.table[i].append(a)
					self.loc[i].append(la)
				else:
					self.table[i].append(b)
					self.loc[i].append(lb)
			idx += 1

	def rmqLocation(self, a, b):
		#Returns the location of rmq(a, b) in ar.
		lg = int(log2(abs(b-a)+1))
		if self.table[a][lg] <= self.table[b-(1<<lg)+1][lg]:
			return self.loc[a][lg]
		else:
			return self.loc[b-(1<<lg)+1][lg]

	def rmq(self, a, b):
		lg = int(log2(abs(b-a)+1))
		return min(self.table[a][lg], self.table[b-(1<<lg)+1][lg])


#tests
# l = [17, 25, 3, 1, 413, 612, 8, 0, 5]
# sp = SparseTable(l)
# print(sp.rmq(1, 4)) #1
# print(sp.rmq(3, 8)) #0
# print(sp.rmq(4, 6)) #8
# print(sp.rmq(2, 2)) #3
# print(sp.rmq(0, 8)) #0
# print()
# print(sp.rmqLocation(1, 4)) #3
# print(sp.rmqLocation(3, 8)) #7
# print(sp.rmqLocation(4, 6)) #6
# print(sp.rmqLocation(2, 2)) #2
# print(sp.rmqLocation(0, 8)) #7

# print(sp.table)